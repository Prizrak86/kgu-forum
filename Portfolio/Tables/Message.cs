﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace Portfolio.Tables
{
    public class Message : Table
    {
        protected static List<RowMessage> Rows = new List<RowMessage>();
        public class RowMessage : Row
        {
            /// <summary>
            /// текст сообщения
            /// </summary>
            public string Text { get; set; }
            /// <summary>
            /// Отправитель
            /// </summary>
            public User.RowUser User { get; set; }
            /// <summary>
            /// Дата время отправки сообщения
            /// </summary>
            public DateTime DateTimeSend { get; set; }
            /// <summary>
            /// Тема
            /// </summary>
            public Thema.RowThema ThemaSend { get; set; }

            public RowMessage(int id) : base(id) { }
        }
        public static int Count()
        {
            return Rows.Count();
        }

        public static void Add(RowMessage th)
        {
            foreach (RowMessage row in Rows)
                if (row.ID == th.ID) return;
            Rows.Add(th);
        }

        public static List<RowMessage> GetMessageForThema(Thema.RowThema thema)
        {
            List<RowMessage> listOut = new List<RowMessage>();
            foreach (RowMessage row in Rows)
            {
                if (row.ThemaSend.ID == thema.ID)
                    listOut.Add(row);
            }
            return listOut;
        }

        public static void Init()
        {
            RowMessage rowMessage = new RowMessage(Rows.Count + 1);
            rowMessage.DateTimeSend = DateTime.Now;
            rowMessage.Text = "Текст сообщения 1";
            rowMessage.ThemaSend = Thema.GetThema(1);
            rowMessage.User = User.GetUser(1);
            Rows.Add(rowMessage);
            Thread.Sleep(1000);

            rowMessage = new RowMessage(Rows.Count + 1);
            rowMessage.DateTimeSend = DateTime.Now;
            rowMessage.Text = "Текст сообщения 2";
            rowMessage.ThemaSend = Thema.GetThema(1);
            rowMessage.User = User.GetUser(2);
            Rows.Add(rowMessage);
            Thread.Sleep(1000);

             rowMessage = new RowMessage(Rows.Count + 1);
            rowMessage.DateTimeSend = DateTime.Now;
            rowMessage.Text = "Текст сообщения 3";
            rowMessage.ThemaSend = Thema.GetThema(2);
            rowMessage.User = User.GetUser(2);
            Rows.Add(rowMessage);
            Thread.Sleep(1000);
             rowMessage = new RowMessage(Rows.Count + 1);
            rowMessage.DateTimeSend = DateTime.Now;
            rowMessage.Text = "Текст сообщения 4";
            rowMessage.ThemaSend = Thema.GetThema(2);
            rowMessage.User = User.GetUser(1);
            Rows.Add(rowMessage);
        }
    }
}