﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portfolio.Tables
{
    public class Role : Table
    {
        protected static List<RowRole> Rows = new List<RowRole>();
        public class RowRole : Row
        {
            public string Name { get; set; }
            public RowRole(int id) : base(id) { Name = ""; }
        }
        public static void InitDefault()
        {
            RowRole rw = new RowRole(1) { Name = "Admin" };
            Rows.Add(rw);
            rw = new RowRole(2) { Name = "User" };
            Rows.Add(rw);

        }
        /// <summary>
        /// Получает строку из таблицы Роли по ключу
        /// </summary>
        /// <param name="name">Ключ поиска роли по имени</param>
        /// <returns></returns>
        public static RowRole GetRole(string name)
        {

            return (from role in Rows where role.Name == name select role).FirstOrDefault();
            
        }

        public static int Count()
        {
            return Rows.Count();
        }
    }
}