﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portfolio.Views.VThema
{
    public partial class Thema : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Tables.Message.Count() == 0)
                Tables.Message.Init();
            if (!Page.IsPostBack)
            {
                string idS = Request.QueryString["id"];
                int id = int.Parse(idS);


                Tables.Thema.RowThema thema = Tables.Thema.GetThema(id);
                Label themaName = new Label();
                themaName.Text = thema.Name;
                PNameThema.Controls.Add(themaName);

                var listMess = Tables.Message.GetMessageForThema(thema);
                foreach (var item in listMess)
                {
                    Panel messPanel = new Panel();
                    //время
                    Panel DTPanel = new Panel();
                    Label DTLabel = new Label();
                    DTLabel.Text = item.DateTimeSend.ToString();
                    DTPanel.Controls.Add(DTLabel);
                    messPanel.Controls.Add(DTPanel);
                    //Пользователь
                    Panel userPanel = new Panel();
                    Label userLabel = new Label();
                    userLabel.Text = item.User.Name;
                    userPanel.Controls.Add(userLabel);
                    messPanel.Controls.Add(userPanel);
                    //Текст
                    Panel textPanel = new Panel();
                    Label textLabel = new Label();
                    textLabel.Text = item.Text;
                    textPanel.Controls.Add(textLabel);
                    messPanel.Controls.Add(textPanel);

                    PContents.Controls.Add(messPanel);
                }
            }

        }
    }
}