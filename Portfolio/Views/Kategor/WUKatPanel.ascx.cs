﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portfolio.Tables;

namespace Portfolio.Views.Kategor
{
    public partial class WUKatPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    if (Kategory.Count() == 0) Kategory.InitDefault();
            //    ListItem[] lic = Kategory.ToListItemCollection();
            //    DropDownList1.Items.Clear();
            //    DropDownList1.Items.AddRange(lic);
            //}
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            Tables.User.RowUser user = (Tables.User.RowUser)Session["User"]; ///получаем из сессии авторизованного пользователя
            if (user != null && user.Role.ID == Tables.Role.GetRole("Admin").ID)///проверка что полдьзовательавторизован и у него есть правва администратора
            {
                int idAddKategory = Kategory.Count() + 1;///Генерируем ИД добавляемой категории
                Kategory.RowKategory row = new Kategory.RowKategory(idAddKategory);///Создаем новую запись категории
                row.Name = TextBox1.Text;///заполняем имя категории введенное пользователем
                Kategory.Add(row);///Добавляем запись в таблицу
                Response.Redirect("~/Views/User/Main.aspx");///переход на страницу
            }
        }



        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}