﻿using Portfolio.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portfolio.Views.User
{
    public partial class User : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Role.Count() == 0) Role.InitDefault();
            Tables.User.RowUser user = (Tables.User.RowUser)Session["User"];
            if (user == null)
                Response.Redirect("~/Views/Login.aspx");
            else if (user.Role.ID == Role.GetRole("Admin").ID)
                Nav1.Visible = true;
        }
    }
}